package ru.t1.schetinin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.api.endpoint.*;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.exception.system.CommandNotSupportedException;
import ru.t1.schetinin.tm.listener.AbstractListener;
import ru.t1.schetinin.tm.event.ConsoleEvent;
import ru.t1.schetinin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.schetinin.tm.util.SystemUtil;
import ru.t1.schetinin.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Getter
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void initCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                //publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("OK");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        boolean commandFound = false;
        for (@NotNull final AbstractListener listener : listeners) {
            if (command.equals(listener.getName())) {
                publisher.publishEvent(new ConsoleEvent(command));
                commandFound = true;
                break;
            }
        }
        if (!commandFound) {
            throw new CommandNotSupportedException(command);
        }
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractListener listener = getCommandByArgument(argument);
        if (listener == null) throw new ArgumentNotSupportedException(argument);
        publisher.publishEvent(new ConsoleEvent(listener.getName()));
    }

    private AbstractListener getCommandByArgument(@Nullable final String argument) {
        for (@NotNull  final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("*** TASK MANAGER IS SHOOTING DOWN ***");
    }

    public void run(@Nullable final String[] args) {
        processArguments(args);
        prepareStartup();
        initCommands();
    }

}