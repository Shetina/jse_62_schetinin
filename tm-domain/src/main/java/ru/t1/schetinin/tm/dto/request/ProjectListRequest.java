package ru.t1.schetinin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.TMSort;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private TMSort sort;

    public ProjectListRequest(@Nullable final String token) {
        super(token);
    }

}