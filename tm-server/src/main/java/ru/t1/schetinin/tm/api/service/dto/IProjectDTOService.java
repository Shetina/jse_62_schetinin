package ru.t1.schetinin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.enumerated.Status;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    void changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

}
